'use strict';

/**
 * Created by Fahmy
 * @ngdoc overview
 * @name koraApp
 * @description
 * # koraApp
 *
 * Main module of the application.
 */
var app = angular
  .module('koraApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',

    'angular-loading-bar',
    'ui.bootstrap',
    'ngDialog',
    'facebook',
    'angular-growl',
    'dndLists',
    'angularFileUpload',
    'angular-ladda',
    'toaster'

  ]);
app.constant('appConfig',
  {
    url: 'http://api.123kora.code95.info',
    apiUrlLeague: 'http://api.123kora.code95.info/api/v1/league?date=',
    apiUrlEvent: 'http://api.123kora.code95.info/api/v1/event?event=',
    apiUrl: 'http://api.123kora.code95.info/api/v1/',
    enLang: '&lang=en&callback=JSON_CALLBACK',
    arLang: '&lang=ar&callback=JSON_CALLBACK'
  });

app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider', 'FacebookProvider', 'ngDialogProvider', 'laddaProvider',
  function ($routeProvider, $locationProvider, cfpLoadingBarProvider, FacebookProvider, ngDialogProvider, laddaProvider) {

    //HashBang
    // $locationProvider.html5Mode({enabled: true, requireBase: true}).hashPrefix('!');

    $routeProvider
      .when('/gamecast/:gameID', {
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl'
      })
      .when('/:date', {
        templateUrl: 'views/matches.html',
        controller: 'MatchsCtrl'
      })
      .when('/', {
        templateUrl: 'views/matches.html',
        controller: 'MatchsCtrl'
      })
      .when('/user/register', {
        templateUrl: 'views/register.html',
        controller: 'registerCtrl'
      })
      .when('user/settings/:userID', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })
      .otherwise({
        templateUrl: '404.html'
      });
    $locationProvider.html5Mode(true);

    //leadingBar
    cfpLoadingBarProvider.includeSpinner = false;


    FacebookProvider
    /**
     * Created by Fahmy
     * @url: 123kora.loc/
     * @app: 123Kora
     * @Autor: Fahmy.farahat
     */
      .init('392105624326370');



    ngDialogProvider
    /**
     * Created by Fahmy
     * ngDialog
     *
     */
      .setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true
      });

    laddaProvider
    /**
     * laddaProvider style
     *
     */
      .setOption({
        style: 'zoom-in'
      });


  }]);

app.run(['$rootScope', 'Lang', function (root, Lang){

  Lang.setLang('ar');
  root.lang = Lang.getTranslation();
}]);

app
  .controller('AppController', ['$rootScope', '$scope', 'authToken', 'Facebook', function ($rootScope,  $scope, authToken, Facebook) {


    //console.log('%c 123KORA your live matches', 'color: red; font-weight:bold; font-size:36px');

    $rootScope.userToKen = authToken.getToken();

    console.log('user token------> ', $rootScope.userToKen);


    $rootScope.isAuthenticated = authToken.isAuthenticated();

    console.log('isAuthenticated-->', $rootScope.isAuthenticated);

    if ($rootScope.isAuthenticated) {
      authToken.getUser($rootScope.userToKen)
        .then(function (data) {
        console.log('Me ', data.user);
        $rootScope.meData = data.user;
        $rootScope.meDataOrderLeague = data.user.orderedLeagues;
        $rootScope.followedLeagues = data.user.followedLeagues;
      }, function (error) {
          console.log('cant reload Me :(', error.status);
        });

    }

    $rootScope.$on('reloadMe', function () {
      if ($rootScope.isAuthenticated) {
        authToken.getUser($rootScope.userToKen)
          .then(function (data) {
            console.log('Me ', data.user);
            $rootScope.meData = data.user;
            $rootScope.meDataOrderLeague = data.user.orderedLeagues;
            $rootScope.followedLeagues = data.user.followedLeagues;
          }, function (error) {
            console.log('cant reload Me :(', error.status);
          });

      }

    });

    $rootScope.logOut = function () {
      authToken.removeToken();
      window.location = '/';
    };



    $rootScope.FBlogin = function() {
        // From now on you can use the Facebook service just as Facebook api says
       //console.log('Facebook', Facebook);
      // Do something with response.
     //console.log('user FB ID: ', response.authResponse.userID);
      $rootScope.loginLoading = true;

          Facebook.login(function () {

            Facebook.api('/me', function (res) {
              $scope.user = res;
              console.log('me',  $scope.user.email, $scope.user.id);

              authToken.FBLogin($scope.user.id, $scope.user.email)
                .success(function (data) {
                  console.log(data.status, data.msg);
                  authToken.setToken(data.token);
                  console.log('posted fine,', data.token);
                  window.location = '/';
                  $rootScope.loginLoading = false;
                }

              ).error(function(err){
                  console.log(err.status, err.msg);
                  alert('خطأ حاول التسجيل مرة اخري');
                  window.location = '/';
                })


            });

          }, {scope: 'public_profile,email'});





    };//end FBLogin..








  }]);

