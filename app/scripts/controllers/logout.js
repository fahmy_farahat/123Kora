'use strict';

/**
 * @ngdoc function
 * @name koraApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the koraApp
 */
angular.module('koraApp')
  .controller('LogoutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
