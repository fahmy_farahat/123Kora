'use strict';

app
/**
 * @ngdoc function
 * @name koraApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the koraApp
 */
  .controller('HeaderCtrl', ['$scope', 'ngDialog', 'appConfig', 'Lang', function ($scope, ngDialog, appConfig, Lang) {

    $scope.imgURL = appConfig.url;


    $scope.settingsPopup = function () {
      var dialog = ngDialog.open({
        template: 'views/dialogs/settings.html',
        className: 'ngdialog-theme-settings',
        controller: 'SettingsCtrl'
      });

    };

    $scope.changelang = function changelang(lang) {
      Lang.setLang(lang);
    };
   /* scope.changelang = function changelang(lang) {
      if(User.getUser()) {
        User.setLang(lang).then(function() {
          Lang.setLang(lang);
          User.getUser().lang = lang;
        });
      } else {
        Lang.setLang(lang);
      }
    };*/


  }]);
