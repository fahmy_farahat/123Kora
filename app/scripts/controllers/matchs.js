'use strict';

/**
 * @ngdoc function
 * @name koraApp.controller:MatchsCtrl
 * @description
 * # MatchsCtrl
 * Controller of the koraApp
 */
app.controller('MatchsCtrl', ['$scope', '$rootScope', 'leaguesFactory', '$routeParams', 'cfpLoadingBar', '$filter', '$location',
  function ($scope, $rootScope, leaguesFactory, $routeParams, cfpLoadingBar, $filter, $location) {

    //Function IsActive url
    $scope.isActive = function (route) {
      return route === $location.path();
    };//End Fun..


    var date = $filter('date')(Date.now(), 'yyyyMMdd');


    //datepicker start -------------
    $scope.dayCalender = moment();
    $scope.isCalenderOpen = true;
    //datepicker End -----


    //carousel Date Start---------------------


    var parseDate = function (str) {
      if (!/^(\d){8}$/.test(str)) {
        return 'invalid date';
      } else {
        var y = str.substr(0, 4),
          m = str.substr(4, 2),
          d = str.substr(6, 2);

        return new Date(y + '-' + m + '-' + d);
      }
    };

    $scope.pathDate = $routeParams.date;

    //console.log('$scope.pathDate: ', $scope.pathDate);


    //Function set today date if url date UNtrue...
    var checkUrlDate = function () {
      var path = $location.path();
      var pathDate = path.substring(1, 9);
      var valYear = path.substring(1, 3);
      var valMonth = path.substring(5, 7);
      var valDay = path.substring(7, 9);
      var valDayNum = parseInt(valDay);
      var valMonNum = parseInt(valMonth);

      if (path.length - 1 !== 8 || valYear !== '20' || pathDate.match(/[^0-9]+/) !== null || valMonNum > 12 || valMonNum === 0 || valDayNum > 31) {
        return $location.path(date);
      }

    }();//end Func..

    var getPathDate = function () {
      var path = $location.path();

      if (path.length > 1) {
        var pathDate = path.substring(1, 9);
        var xDate = parseDate(pathDate);
        return xDate;
      } else {
        var time = new Date();
        return time;
      }

    };


    //   var time = new Date();
    //   //console.log('time : ', time);

    var varOfGetPathDate = getPathDate();
    //console.log('xDate', varOfGetPathDate);


    var pathDateTime = varOfGetPathDate.getTime();
    //console.log('pathDateTime : ', pathDateTime);

    var startTime = pathDateTime - (2 * 1000 * 60 * 60 * 24);

    var startSetTime = new Date();

    startSetTime.setTime(startTime);
    //console.log('startTime : ', startSetTime);

    $scope.labels = new Array(11);
    for (var j = 0; j < 11; j++) {
      var oneDay = pathDateTime + (j * 1000 * 60 * 60 * 24) - (5 * 1000 * 60 * 60 * 24);
      var setTime = new Date();
      setTime.setTime(oneDay);
      $scope.labels[j] = setTime;
    }


    //function set mid of navigation date

    var setMiddDateArr = function (midDataArr) {
      //set middle of array date in url
      midDataArr;//get mid date from array
      var setDate = new Date();
      setDate.setTime(midDataArr);
      var gDay = setDate.getDate(), gMonth = setDate.getMonth() + 1, gYear = setDate.getFullYear();
      if (gDay < 10) {
        gDay = '0' + gDay;
      }
      if (gMonth < 10) {
        gMonth = '0' + gMonth;
      }

      var getFullDates = gYear + '' + gMonth + '' + gDay;
      //console.log('midDataArr: ', getFullDates);

      return $location.path(getFullDates);//set data in url
    };//end Fun


    //Function: Next Date in Carousel
    $scope.nextDate = function () {
      $scope.labels.shift();
      var valOfLastArr = $scope.labels[9];
      var dateObj = new Date();
      var dateMoreObj = new Date();

      dateObj.setTime(valOfLastArr);
      var moreDay = dateObj.getTime() + (1000 * 60 * 60 * 24);
      //console.log('valOfLastArr With a Day', moreDay);

      $scope.labels.push(dateMoreObj.setTime(moreDay));
      //console.log('$scope.labels', $scope.labels);

      setMiddDateArr($scope.labels[5]);


    };//End Fun..


    //function: previous date in carousel
    $scope.prevDate = function () {
      var dateArr = $scope.labels;//get array of dates.
      var valFirstArr = dateArr[0]; //get first elemnt..
      //remove last elemnt of an array
      dateArr.pop();
      //console.log('firstArray val: ', valFirstArr + (1000 * 60 * 60 * 24));

      var dateObj = new Date();
      var dateMoreObj = new Date();

      dateObj.setTime(valFirstArr);
      var moreDay = dateObj.getTime() - (1000 * 60 * 60 * 24);
      //console.log('valFirstArr With a Day', moreDay);
      dateArr.unshift(dateMoreObj.setTime(moreDay));
      //console.log('dateArr', dateArr);

      setMiddDateArr(dateArr[5]);
    };//end Fun..


    //carousel Date End ----------------


    $scope.date = date;
    //console.log('routeParams.date: ', $routeParams.date);
    //console.log('typeof routeParams.date: ', typeof ($routeParams.date));

    //Function init today date in home page
    var initDate = function () {
      if (typeof $routeParams.date === 'undefined') {
        var setDate = $location.path(date);
        ////console.log('setDate: ', setDate);
        return setDate;
      }
    }();//End init Function...

//console.log('$rootScope.isAuthenticated matches ---> ',  $rootScope.isAuthenticated);
    $rootScope.$on('reloadLeagues', function() {

      if ($rootScope.isAuthenticated) {
        //Start Loading...
        $scope.loading = true;
        //get Matches From API..
        leaguesFactory.getleaguesMatchs($routeParams.date + '&token=' + $rootScope.userToKen)
          .then(function (response) {
            console.log('response: ' + response);
            cfpLoadingBar.start();
            $scope.leagues = response.data;
            cfpLoadingBar.complete();
            $scope.loading = false;
          });

      } else {

        //Start Loading...
        $scope.loading = true;
        //get Matches From API..
        leaguesFactory.getleaguesMatchs($routeParams.date)
          .then(function (response) {
            cfpLoadingBar.start();
            $scope.leagues = response.data;
            cfpLoadingBar.complete();
            $scope.loading = false;
          });
      }

    });

    if ($rootScope.isAuthenticated) {
      //Start Loading...
      $scope.loading = true;
      //get Matches From API..
      leaguesFactory.getleaguesMatchs($routeParams.date + '&token=' + $rootScope.userToKen)
        .then(function (response) {
          console.log('response: ' + response);
          cfpLoadingBar.start();
          $scope.leagues = response.data;
          cfpLoadingBar.complete();
          $scope.loading = false;
        });

    } else {

      //Start Loading...
      $scope.loading = true;
      //get Matches From API..
      leaguesFactory.getleaguesMatchs($routeParams.date)
        .then(function (response) {
          cfpLoadingBar.start();
          $scope.leagues = response.data;
          cfpLoadingBar.complete();
          $scope.loading = false;
        });
    }


  }]);
