'use strict';


app
/**
 * Created by Fahmy
 * @ngdoc function
 * @name koraApp.controller:registerCtrl
 * @description
 * # registerCtrl
 * Controller of the koraApp
 */
  .controller('registerCtrl', ['$scope', 'appConfig', '$http', 'growl', 'authToken', 'Facebook',
    function ($scope, appConfig, $http, growl, authToken, Facebook) {

      //console.clear();
      console.log(appConfig.apiUrl);


      $scope.register = function () {
        var inputs = $scope.register_data;


        $scope.register_errors = null;
        var strUserName = inputs.user_email;
        var userName = strUserName.replace(/@.*$/, "");
        //console.log(userName);


        var userDataPar = 'name=' + userName + '&email=' + inputs.user_email +
          '&password=' + inputs.user_password + '&password_confirmation=' + inputs.user_password + '&mobile=' + inputs.user_phone;

        console.log(userDataPar);

        var url = appConfig.apiUrl + 'user/register?' + userDataPar;

        $http.post(url)
          .success(function (res) {

            growl.success('تم التسجيل بنجاح');
            authToken.setToken(res.token);
            console.log('posted fine,', res.token);
            window.location = '/';

          })
          .error(function (err) {
            $scope.errData = err;
            console.log($scope.errData.msg, $scope.errData.status);

            if ($scope.errData.status == '400') {
              growl.error('البريد الالكتروني مستخدم من قبل!');
            }

          })

      };

      $scope.registered = false;


      /**
       * Facebook Register
       *
       */
      $scope.fbRegister = function () {


        $scope.registered = true;

        Facebook.login(function () {

          Facebook.api('/me', function(response) {

            $scope.user = response;
            console.log('me',  $scope.user.email, $scope.user.id);

            var phoneNumber =   prompt('يجب ادخال رقم الجوال لاتمام عملية التسجيل');

            if(phoneNumber){

              authToken.FBRegister($scope.user.id, $scope.user.email, phoneNumber)
                .success(function (data) {
                  console.log(data.status, data.msg);
                  authToken.setToken(data.token);
                  console.log('posted fine,', data.token);
                  window.location = '/';

                  $scope.registered = false;


                }

              ).error(function(err){
                  console.log(err.status, err.msg);
                  alert('فشل عملية التسجيل');
                  $scope.registered = false;

                })
            }else{
              alert('يجب ادخال رقم الهاتف لاتمام عملية التسجيل');
              window.location = '/user/register';
            }




          });


        }, {scope: 'public_profile,email'});



      };



    }]);

app
/**
 * Created by Fahmy
 * @ngdoc function
 * @name koraApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the koraApp
 */
  .controller('LoginCtrl', ['$scope', 'appConfig', '$http', 'authToken', 'growl', function ($scope, appConfig, $http, authToken, growl) {


    $scope.login = function () {

      var inputs = $scope.loginData;

      var UserDataLogin = 'email=' + inputs.user_email + '&password=' + inputs.user_password;


      console.log(UserDataLogin);

      var url = appConfig.apiUrl + 'user/login?' + UserDataLogin;

      $http.post(url)
        .success(function (res) {
          //alert('success', 'تم', 'التسجيل');
          authToken.setToken(res.token);
          console.log('posted fine,', res.token);
          var pathName = window.location.pathname;
          console.log(pathName);
          if (pathName == '/user/register') {
            window.location = '/';
          } else {
            return window.location = pathName;
          }
        })
        .error(function (err) {
          $scope.errData = err;
          console.log($scope.errData.msg, $scope.errData.status);

        });


    };

  }]);


app
/**
 * Created by Fahmy
 * @ngdoc function
 * @name koraApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the koraApp
 */
  .controller('SettingsCtrl', ['$scope', 'authToken', 'appConfig', '$upload', '$rootScope',
    function ($scope, authToken, appConfig, $upload, $rootScope) {

    //console.log(window.location.pathname);
    $scope.imgURL = appConfig.url;


    $scope.$watch('files', function () {
      $scope.upload($scope.files);
    });


    var urlAPI = appConfig.apiUrl + 'user/profile?token=' + authToken.getToken();


    $scope.upload = function (files) {
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          $upload.upload({
            url: urlAPI,

            file: file,
            fileFormDataName: 'photo',
          }).progress(function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' +
            evt.config.file.name);
          }).success(function (data, status, headers, config) {
            console.log('file ' + config.file.name + 'uploaded. Response: ' +
            JSON.stringify(data));
          });
        }
      }
    };


    // Model to JSON for demo purpose
    /* $scope.$watch('meDataOrderLeague', function(model) {
     $scope.ordered_leagues = angular.toJson(model, true);


     }, true);*/

    var meOrderLeag = $rootScope.meData.followedLeagues;
    console.log('meOrderLeag', meOrderLeag.length);
   console.log('meDataOrderLeague: ', $scope.followedLeagues);

    // Model to array
    $scope.$watch('followedLeagues', function (model) {
      $scope.ordered_leagues = model;

      var newArr = [];
      for (var xID = 0; xID < $scope.ordered_leagues.length; xID++) {
        var strID = $scope.ordered_leagues[xID]._id;
        newArr.push(strID);
      }

      if (newArr.length == meOrderLeag.length) {
        console.log('newArr ', typeof(newArr), newArr);

        var jsstring = JSON.stringify(newArr);
        console.log('newArr ', typeof(jsstring), jsstring);


        authToken.orderedLeagues(authToken.getToken(), jsstring)
          .success(function (res) {
            console.log('leagues order has been sent', res.status, res.response, res.user);
          })
          .error(function (err) {
            console.log('error with ordered leagues', err.status, err.msg);
          })


      }


    }, true);


    $scope.editUserProfile = function (data) {


      var input = 'name=' + data.user_name + '&email=' + data.email + '&mobile=' + data.mobile;
      console.log('form Data: ', input);


      authToken.profileUpdate(authToken.getToken(), input)
        .success(function (res) {
          $scope.userEdite = res

          console.log('data has been edited: ', res.status);
        })
        .error(function (err) {
          console.log('error with profile edit..', err.status);
        });
    };

      $scope.windowReload = function windowReload () {
        window.location = window.location.pathname;
      };

      $scope.unHidded = false;

      /**
       * un hide Leagues function
       *
       */
      $scope.unHiddeLeague = function unHiddeLeague (leagueID) {

        authToken.hiddeLeague(authToken.getToken(), leagueID, false)
          .success(function (res) {
            console.log('league has been unhidde ;)', res.status);
           // window.location = window.location.pathname;
            $rootScope.$broadcast('reloadMe');
            $rootScope.$broadcast('reloadLeagues');


          }).error(function (err) {

            console.log('Error with unhide league :( ', err.status);

          });

      };

      /**
       * Hide Leagues function
       *
       */
      $scope.hideLeague = function hideLeague (leagueID) {

        authToken.hiddeLeague(authToken.getToken(), leagueID, true)
          .success(function (res) {
            console.log('league has been hide ;)', res.status);
            //window.location = window.location.pathname;
            $rootScope.$broadcast('reloadMe');
            $rootScope.$broadcast('reloadLeagues');

          }).error(function (err) {

            console.log('Error with hide league :( ', err.status);

          });

      };

  }]);


