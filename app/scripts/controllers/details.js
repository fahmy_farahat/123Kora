'use strict';

/**
 * @ngdoc function
 * @name koraApp.controller:DetailsCtrl
 * @description
 * # DetailsCtrl
 * Controller of the koraApp
 */
app.controller('DetailsCtrl', ['$scope', '$routeParams', 'gamecastFactory', 'cfpLoadingBar', 'appConfig', function ($scope, $routeParams, gamecastFactory, cfpLoadingBar, appConfig) {

  console.log('routeParams: ', $routeParams.gameID);

  $scope.tab = 'tap2';

  $scope.setTab = function (tab) {
    $scope.tab = tab;
  };

  $scope.isSet = function (tab) {
    return $scope.tab === tab;
  };


  //Start Loading...
  $scope.loading = true;
  //get event From API..
  gamecastFactory.getGameEvent($routeParams.gameID)
    .then(function (response) {
      /*
      console.log('response: ' + response);
      console.log('API Status: ' + response.status);
      console.log('Type: ' + response.headers('content-type'));
      console.log('Length: ' + response.headers('content-length'));
      */

      cfpLoadingBar.start();
      $scope.gameEvent = response.data;
      $scope.pbpObj = _(response.data.pbp).reverse().value();
      cfpLoadingBar.complete();
      $scope.loading = false;
      $('.scrollme').mCustomScrollbar();

      $scope.periodDetails = response.data.period_details;

      console.log('period_details ', $scope.periodDetails);

      //Start own Object...
      var data = response.data;



      var teamHome = {
        id            : data.teams[0].teamId,
        name          : data.teams[0].displayName,
        abbreviation  : data.teams[0].abbreviation,
        locationType  : data.teams[0].teamLocationType.name,
        formation     : data.teams[0].formation,
        shots         : data.teams[0].shots,

      };

      var teamAway = {
        id            : data.teams[1].teamId,
        name          : data.teams[1].displayName,
        abbreviation  : data.teams[1].abbreviation,
        locationType  : data.teams[1].teamLocationType.name,
        formation     : data.teams[1].formation,
        shots         : data.teams[1].shots,

      };



      console.log('team Home: ', teamHome, teamAway);







    });



}]);
