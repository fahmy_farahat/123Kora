'use strict';

/**
 * @ngdoc function
 * @name koraApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the koraApp
 */
angular.module('koraApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
