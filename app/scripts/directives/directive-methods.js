'use strict';
app
/**
 * Simple directive to check password Match
 *
 * usage:
 * <input type="password" ng-model="password" password-match="password2">
 * <input type="password" ng-model="password2">
 */
  .directive('passwordMatch', function () {
    return {
      restrict: 'A',
      scope: false,
      require: 'ngModel',
      link: function (scope, elem, attrs, controller) {
        var checker = function () {
          // get the value of the first password
          var pwd = scope.$eval(attrs.ngModel);
          // get the value of the other password
          var pwd2 = scope.$eval(attrs.passwordMatch);
          return pwd === pwd2;
        };
        scope.$watch(checker, function (pwdMatch) {
          controller.$setValidity('match', pwdMatch);
        });
      }
    };
  });
