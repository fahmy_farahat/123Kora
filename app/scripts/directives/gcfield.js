'use strict';

/**
 * @ngdoc directive
 * @name koraApp.directive:gcField
 * @description
 * # gcField
 */
(function () {

  function controller(scope){

    var pitch = d3.select('#pitch');

    pitch
      .append('svg') .style("background", "url(images/pitch.svg)")
      //.attr("xlink:href", 'images/pitch.svg')
      //.attr("x", "60")
      //.attr("y", "60")
      .attr("width", "654")
      .attr("height", "329");
    console.log(pitch);



  }

  angular.module('koraApp')
    .directive('gcField', function () {
      return {
        templateUrl: 'views/directives/gc-field.html',
        restrict: 'E',
        controller: ['$scope', controller]
      };
    });
}).call(null);
