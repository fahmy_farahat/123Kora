'use strict';

/**
 * @ngdoc directive
 * @name koraApp.directive:gcTimeline
 * @description
 * # gcTimeline
 */
(function(){

  function controller(scope){


    var watcher = scope.$watch('details', function () {

      if(! _.isUndefined(scope.details)){
        watcher
        console.log('details: ', scope.details);


      }

    });



   var timeLine = d3.select('#timeline');

    timeLine
      .append('svg') .style("background", "url(images/grey_timeline_unit.svg)")
      .style('background-repeat', 'repeat-x')
      .style('background-size', '40px 20px')
      //.attr("xlink:href", 'images/pitch.svg')
      //.attr("x", "60")
      //.attr("y", "60")
      .attr("width", "100%")
      .attr("height", "20");



  }

  angular.module('koraApp')
    .directive('gcTimeline', function () {
      return {
        template: '<div id="timeline" style="direction: ltr"></div>',
        restrict: 'E',
        scope: {
          details: '='
        },
        controller: ['$scope', controller]
      };
    });
}).call(null);
