'use strict';

app
/**
 * Created by Fahmy
 * @ngdoc service
 * @name koraApp.FollowedLeagues
 * @description
 * # FollowedLeagues
 * Service in the koraApp.
 */
  .factory('FollowedLeagues', ['appConfig', '$http', '$q', function (appConfig, $http, $q) {

    var FollowedLeagues = {};

    FollowedLeagues.follow = function (token, leagueID) {
      return $http.put(appConfig.apiUrl + 'user/follow-league?token=' + token + '&league_id=' + leagueID + '&status=true')
        .then(function (response) {

          if (typeof response.data === 'object') {
            return response.data;
          } else {
            // invalid response
            return $q.reject(response.data);
          }

        },
        function (response) {
          // something went wrong
          return $q.reject(response.data);
        });
    };

    FollowedLeagues.unfollow = function (token, leagueID) {
      return $http.put(appConfig.apiUrl + 'user/follow-league?token=' + token + '&league_id=' + leagueID + '&status=false')
        .then(function (response) {

          if (typeof response.data === 'object') {
            return response.data;
          } else {
            // invalid response
            return $q.reject(response.data);
          }

        },
        function (response) {
          // something went wrong
          return $q.reject(response.data);
        });
    };


    return FollowedLeagues;

  }]);

app
/**
 * @ngdoc directive
 * @name koraApp.directive:fbFollowBtn
 * @description
 * # fbFollowBtn
 */
  .directive('fbFollowBtn', ['$rootScope', '$compile', 'FollowedLeagues', function ($rootScope, $compile, FollowedLeagues) {


    var getLeagueID = function (leagueID, followed) {

      if (typeof(followed) !== 'undefined') {
        for (var i = 0; i < followed.length; i++) {
          var fLeagues = followed[i]._id;
          if (fLeagues === leagueID) {
            return fLeagues;
          }

        }//End-Loop
      }//End-if

    };//End-function.


    return {
      restrict: 'A',

      link: function (scope, element, attrs) {

        scope.followed = $rootScope.followedLeagues;
        scope.leagueid = attrs.leagueid;

        var followBtn = null;
        var unfollowBtn = null;

        var createFollowBtn = function () {

          followBtn = angular.element('<a href="javascript:void(0)" class="btn-danger btn" ng-disabled="submitting">Follow</a>');

          $compile(followBtn)(scope);
          element.append(followBtn);

          followBtn.bind('click', function () {
            scope.submitting = true;
            FollowedLeagues.follow($rootScope.userToKen, scope.leagueid)
              .then(function () {
                scope.submitting = false;
                followBtn.remove();
                createUnfollowBtn();

                //console.log('followed Leagues Done :-) ', data);
              });
            // scope.$apply();

          });

        };


        var createUnfollowBtn = function () {
          unfollowBtn = angular.element('<a href="javascript:void(0)" class="btn-default btn" ng-disabled="submitting">Unfollow</a>');
          $compile(unfollowBtn)(scope);
          element.append(unfollowBtn);

          unfollowBtn.bind('click', function () {
            scope.submitting = true;

            FollowedLeagues.unfollow($rootScope.userToKen, scope.leagueid)
              .then(function () {
                scope.submitting = false;
                unfollowBtn.remove();
                createFollowBtn();

                //console.log('followed Leagues Done :-) ', data);
              });
            // scope.$apply();
          });


        };


        scope.$watch('leagueid', function () {

          var leag = getLeagueID(scope.leagueid, scope.followed);


          if (typeof(leag) === 'undefined') {

            createFollowBtn();

          } else if (typeof(leag) !== 'undefined') {
            createUnfollowBtn();
          }//end if


        });


      }
    };
  }]);
