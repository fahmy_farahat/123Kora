/**
 * Created by Fahmy on 3/29/15.
 */
window.KORA_ENGLISH = {
  AR                                      : 'ع',
  EN                                      : 'EN',

  USER_NAME                               : 'الاسم',
  USER_MAIL                               : 'البريد الالكتروني',
  NAME_REQUIRED                           : 'الاسم الخاص بك مطلوب لاتمام عملية التسجيل',
  USER_EMAIL_UNIQUE                       : 'البريد الالكتروني مستخدم من قبل مستخدم أخر',
  EMAIL_REQUIRED                          : 'البريد الالكتروني الخاص بك مطلوب لاتمام عمليه التسجيل',
  INVALID_EMAIL                           : 'تأكد من كتابه بريدك اﻷلكتروني بشكل صحيح',
  YOUR_PASSWORD                           : 'كلمة المرور',
  YOUR_PASSWORD_AGAIN                     : 'تاكيد كلمة المرور',
  PASSWORD_REQUIRED                       : 'يجب كتابه كلمه مرور لاكمال عمليه التسجيل',
  PASSWORD_SHORT                          : 'كلمه المرور لا يجب ان تقل عن ٨ حروف',
  PHONE_REQUIRED                          : 'رقم الجوال مطلوب لاتمام عملية التسجيل',
  PHONE_SHORT                             : '',
  PHONE_NUMBER                            : '',
  MY_ACCOUNT                              : 'My Account',
  HELP_ME                                 : 'Help',
  SIGNOUT                                 : 'SignOut',
  SETTINGS                                : 'Settings',
  CURRENT_PASSWORD                        : '',
  NEW_PASSWORD                            : '',
  CHANGE_PASSWORD                          : '',
  SAVE_BTN                                : 'حفظ',
  CANCEL_BTN                              : 'إلغاء',
  ACCOUNT_TAP_TITLE                       : 'الحساب',
  LEAGUES_TAP_TITLE                       : 'الدوريات',
  DONE                                    : 'تم',
  INVALID_LOGIN                           : 'خطأ في البريد الالكتروني او كلمة المرور',
  THENUMBER                               :'الرقم',
  PLAYER_NAME                             : 'اسم اللاعب',
  PLAYER_POSTION                          : 'المركز',
  HIDDEN_LEAGUES                          : 'الدوريات المخفية',
  NO_LEAGUES                              : 'لا يوجد دوريات',
  SHOW_LEAGUE                             : 'إظهار الدوري'

};
