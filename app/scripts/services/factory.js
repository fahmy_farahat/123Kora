'use strict';

app
/**
 * @ngdoc service
 * @name koraApp.leaguesFactory
 * @description
 * # leaguesFactory
 * Service in the koraApp.
 */
  .factory('leaguesFactory', ['$http', 'appConfig', function ($http, appConfig) {

    var leaguesFactory = {};

    leaguesFactory.getleaguesMatchs = function (date) {
      return $http.jsonp(appConfig.apiUrlLeague + date + appConfig.arLang);
    };

    return leaguesFactory;


  }]);

app
/**
 * @ngdoc service
 * @name koraApp.gamecastFactory
 * @description
 * # gamecastFactory
 * Service in the koraApp.
 */
  .factory('gamecastFactory', ['$http', 'appConfig', function ($http, appConfig) {

    var gamecastFactory = {};

    gamecastFactory.getGameEvent = function (date) {
      return $http.jsonp(appConfig.apiUrlEvent + date + '&callback=JSON_CALLBACK');
    };

    return gamecastFactory;


  }]);

app
/**
 * @ngdoc service
 * @name koraApp.alert
 * @description
 * # alert
 * Service in the koraApp.
 */
  .factory('alert', ['$rootScope', '$timeout', function ($rootScope, $timeout) {

    var alertTimeout;

    return function (type, title, message, timeout) {

      $rootScope.alert = {
        hasBeenShown: true,
        show: true,
        type: type,
        message: message,
        title: title
      };

      $timeout.cancel(alertTimeout);
      alertTimeout = $timeout(function () {
        $rootScope.alert.show = false;
      }, timeout || 2000);


    };

  }]);


app
/**
 * Created by Fahmy
 * @ngdoc service
 * @name koraApp.authToken
 * @description
 * # authToken
 * Service in the koraApp.
 */
  .factory('authToken', ['$window', '$http', 'appConfig', '$q', function ($window, $http, appConfig, $q) {

    var storage = $window.localStorage;
    var checkedToken;
    var userToken = 'userToken';

    var authToken = {};

    authToken.setToken = function (token) {
      checkedToken = token;
      storage.setItem(userToken, token);
    };

    authToken.getToken = function () {
      checkedToken = storage.getItem(userToken);
      return checkedToken;
    };

    authToken.removeToken = function () {
      checkedToken = null;
      storage.removeItem(userToken);
    };

    authToken.isAuthenticated = function () {
      return !!authToken.getToken();
    };

    /*authToken.loadMe = function(token){
     return $http.jsonp(appConfig.apiUrl+'user/me?token='+ token + '&callback=JSON_CALLBACK');
     };*/

    authToken.profileUpdate = function (token, input) {
      return $http.post(appConfig.apiUrl + 'user/profile?token=' + token + '&' + input);
    };

    authToken
    /**
     * Ordered Leagues.
     * @param {string} token User JWT Token.
     * @param {string} orderedArray JSON,
     *  array of league IDs
     */
      .orderedLeagues = function (token, orderedArray) {
      return $http.put(appConfig.apiUrl + 'user/ordered-leagues?token=' + token + '&ordered_leagues=' + orderedArray);
    };

    authToken
    /**
     * Hide & Show Leagues.
     * @param {string} token User JWT Token.
     * @param {string|number} leagueID League ID.
     * @param {boolean} status True to hide and,
     *  False to show.
     */
      .hiddeLeague = function (token, leagueID, status) {
      return $http.put(appConfig.apiUrl + 'user/hide-league?token=' + token + '&league_id=' + leagueID + '&status=' + status);
    };

    authToken.getUser = function (token) {
      return $http.jsonp(appConfig.apiUrl + 'user/me?token=' + token + '&callback=JSON_CALLBACK')
        .then(function (response) {

          if (typeof response.data === 'object') {
            return response.data;
          } else {
            // invalid response
            return $q.reject(response.data);
          }

        },
        function (response) {
          // something went wrong
          return $q.reject(response.data);
        });
    };

    authToken.FBRegister = function (FBid, FBmail, mobile){
      return $http.post(appConfig.apiUrl + 'user/fb-register?fb_id=' + FBid +'&email='+ FBmail +'&mobile='+ mobile )
        .then(function (response) {

          if (typeof response.data === 'object') {
            return response.data;
          } else {
            // invalid response
            return $q.reject(response.data);
          }

        },
        function (response) {
          // something went wrong
          return $q.reject(response.data);
        });

    };

    authToken.FBLogin = function (userID, userMail) {
      return $http.post(appConfig.apiUrl + 'user/fb-login?fb_id=' + userID +'&email='+ userMail);
    };

    authToken.FBRegister = function (userID, userMail, phoneNumber) {
      return $http.post(appConfig.apiUrl + 'user/fb-register?fb_id=' + userID +'&email='+ userMail +'&mobile='+phoneNumber );
    };

    return authToken;

  }]);
