'use strict';

/**
 * @ngdoc service
 * @name koraApp.lang
 * @description
 * # lang
 * Service in the koraApp.
 */
(function(){

  function service() {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var english = window.KORA_ENGLISH;
    var arabic  = window.KORA_ARABIC;
    _.each(english, function(v, k) {
      if (!arabic[k]) {
        arabic[k] = 'ar_' + v;
        console.log("Arabic translation missing " + k + " : " + v);
      }
    });
    _.each(arabic, function(v, k) {
      if (!english[k]) {
        english[k] = 'en_' + v;
        console.log("English translation missing " + k + " : " + v);
      }
    });
    this.lang = 'en';
    this.translations = {
      en: english,
      ar: arabic
    };

    this.setLang = function setLang(lang) {
      this.lang = lang;
    };

    this.getLang = function getLang() {
      return this.lang;
    };

    this.getLangs = function getLangs() {
      var trans = this.getTranslation();
      return _.map(_.keys(this.translations), function(langKey) {
        return {id: langKey, name: trans[langKey]};
      });
    };

    this.getTranslation = function getTranslation() {
      return this.translations[this.lang];
    };

    var that = this;

    this.$get = ['$rootScope', function LangFactory(root) {
      function LangService() {
        this.getTranslation = function() {
          if (!that) {
            console.log("Lang not loaded");
            return;
          }
          var ret;
          if (_.isUndefined(that.lang)){
            that.lang = 'en';
          }
          ret = that.translations[that.lang];
          // Having all translations all the time
          ret.trans_obj = that.translations;
          ret.i18n = this.i18n;
          return ret;
        };

        this.getLang = function(lang) {
          return that.lang;
        };

        this.setLang = function(lang) {
          if (that.lang === lang) {
            return;
          }
          that.lang = lang;
          if (lang === 'ar') {
            root.LANG_DIRECTION = 'rtl';
          } else {
            root.LANG_DIRECTION = 'ltr';
          }
          root.$emit('lang_updated');
        };

        this.valueOf = function valueOf(key, lang) {
          return that.translations[lang || that.lang][key];
        };

        this.i18n = function i18n(obj, attr) {
          if (!obj) {
            return '';
          }
          if (!attr) {
            console.log("Trying to internationalize undefined attr", obj);
            return 'Undefined';
          }
          var key = attr + '_' + that.lang;
          var res = obj[key] || obj[attr];
          //console.log("i18n [ " + key +  " ] : " + res );
          return res;
        };
      }
      return new LangService();
    }];

  }

  angular.module('koraApp')
    .provider('Lang', service);
}).call(null);
