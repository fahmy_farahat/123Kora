/* Rules for top section after & before scroll */
$(window).scroll(function () {
  if ($(document).scrollTop() > 102) {
    $('#top-logo.navbar-brand').css({'display': 'inline-block'});
    $('.right-menu').hide();
  } else {
    $('#top-logo.navbar-brand').hide();
    $('.right-menu').fadeIn();
    $('#loading-bar .bar').css({'top': '50px'})
  }
});
/* search box */
$(document).ready(function () {
  var submitIcon = $('.searchbox-icon');
  var inputBox = $('.searchbox-input');
  var searchBox = $('.searchbox');
  var isOpen = false;
  submitIcon.click(function () {
    if (isOpen == false) {
      searchBox.addClass('searchbox-open');
      inputBox.focus();
      isOpen = true;
    } else {
      searchBox.removeClass('searchbox-open');
      inputBox.focusout();
      isOpen = false;
    }
  });
  submitIcon.mouseup(function () {
    return false;
  });
  searchBox.mouseup(function () {
    return false;
  });
  $(document).mouseup(function () {
    if (isOpen == true) {
      $('.searchbox-icon').css('display', 'block');
      submitIcon.click();
    }
  });


});
function buttonUp() {
  var inputVal = $('.searchbox-input').val();
  inputVal = $.trim(inputVal).length;
  if (inputVal !== 0) {
    $('.searchbox-icon').css('display', 'none');
  } else {
    $('.searchbox-input').val('');
    $('.searchbox-icon').css('display', 'block');
  }
}


