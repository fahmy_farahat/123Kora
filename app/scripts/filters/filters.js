'use strict';

/**
 * @ngdoc filter
 * @name koraApp.filter:filters
 * @function
 * @description
 * # filters
 * Filter in the koraApp.
 */
app.filter('matchORmatchs', function () {

  return function (match) {
    if (match !== 1) {
      return 'مباريات';
    }
    else {
      return 'مباراة';
    }
  };

});


app.filter('reverse', function () {
  return function (items) {
    return items.slice().reverse();
  };
});
