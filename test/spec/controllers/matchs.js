'use strict';

describe('Controller: MatchsCtrl', function () {

  // load the controller's module
  beforeEach(module('koraApp'));

  var MatchsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MatchsCtrl = $controller('MatchsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
