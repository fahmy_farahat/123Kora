'use strict';

describe('Directive: gcTimeline', function () {

  // load the directive's module
  beforeEach(module('koraApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<gc-timeline></gc-timeline>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the gcTimeline directive');
  }));
});
