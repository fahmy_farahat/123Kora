'use strict';

describe('Directive: gcField', function () {

  // load the directive's module
  beforeEach(module('koraApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<gc-field></gc-field>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the gcField directive');
  }));
});
